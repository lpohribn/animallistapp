//
//  FactsModel.swift
//  TechTaskGenesis
//
//  Created by Liudmila on 11.02.2022.
//

import Foundation

struct Fact {
    let fact: String
    let image: String
}
