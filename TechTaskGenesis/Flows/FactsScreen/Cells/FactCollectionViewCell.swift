//
//  FactCollectionViewCell.swift
//  TechTaskGenesis
//
//  Created by Liudmila on 13.02.2022.
//

import UIKit

class FactCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    private var path: String?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageView.image = nil
    }
    
    func setupCell(imgPath: String, text: String) {
        self.path = imgPath
        indicator.startAnimating()
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            if let url = URL(string: imgPath),
               let data = try? Data(contentsOf: url),
               let image = UIImage(data: data),
               imgPath == self?.path {
                DispatchQueue.main.async {
                    self?.imageView.image = image
                    self?.indicator.stopAnimating()
                }
            } else {
                DispatchQueue.main.async {
                    self?.imageView.image = UIImage(systemName: "photo.on.rectangle")
                    self?.indicator.stopAnimating()
                }
            }
        }
        label.text = text
    }
    
}
