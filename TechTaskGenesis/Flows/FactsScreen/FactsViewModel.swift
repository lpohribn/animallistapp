//
//  FactsViewModel.swift
//  TechTaskGenesis
//
//  Created by Liudmila on 11.02.2022.
//

import Foundation

final class FactsViewModel {

    private var fact: [Fact] = []
    private let networkService: NetworkService?
    
    init(array: [Content]?, networkService: NetworkService) {
        self.networkService = networkService
        if let factArray = array {
            for element in factArray {
                fact.append(Fact(fact: element.fact, image: element.image))
            }
        }
    }
    
    func getContent() -> [Fact] {
        return fact
    }
    
    func itemAtIndexPath(_ indexPath: IndexPath) -> Fact {
        return fact[indexPath.row]
    }
    
    func countOfFacts() -> Int {
        return fact.count
    }
}
