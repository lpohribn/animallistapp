//
//  FactsViewController.swift
//  TechTaskGenesis
//
//  Created by Liudmila on 10.02.2022.
//

import UIKit

final class FactsViewController: UIViewController {

    @IBOutlet weak var factCollectionView: UICollectionView!
    @IBOutlet weak var previosSlideButton: UIButton!
    @IBOutlet weak var nextSlideButton: UIButton!
    
    private var viewModel: FactsViewModel?
    
    var currentIndex = 0
    
   
    init?(coder: NSCoder, viewModel: FactsViewModel) {
       self.viewModel = viewModel
       super.init(coder: coder)
     }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        if let navBar = navigationController?.navigationBar {
            navBar.tintColor = .black
            navigationItem.title = "Facts"
        }
        
        factCollectionView.register(UINib.init(nibName: "FactCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "factCollectionViewCellIdentifier")
        factCollectionView.dataSource = self
        factCollectionView.delegate = self
    }
    
    @IBAction func previousButtonPressed(_ sender: UIButton) {
        if currentIndex != 0 {
            self.factCollectionView.scrollToItem(at: IndexPath(row: currentIndex - 1, section: 0), at: .centeredHorizontally, animated: true)
            currentIndex -= 1
        }
    }
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        if currentIndex + 1 < viewModel?.countOfFacts() ?? 0 {
            self.factCollectionView.scrollToItem(at: IndexPath(row: currentIndex + 1, section: 0), at: .centeredHorizontally, animated: true)
            currentIndex += 1
        }
    }
    
}

extension FactsViewController: UICollectionViewDataSource,
                               UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 10, height: collectionView.frame.height - 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.getContent().count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = factCollectionView.dequeueReusableCell(withReuseIdentifier: "factCollectionViewCellIdentifier", for: indexPath) as? FactCollectionViewCell {
            cell.setupCell(imgPath: self.viewModel?.itemAtIndexPath(indexPath).image ?? "", text: self.viewModel?.itemAtIndexPath(indexPath).fact ?? "")
            return cell
        }
        return UICollectionViewCell()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout
                        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 0, right: 5)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: factCollectionView.contentOffset, size: factCollectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        if let visibleIndexPath = factCollectionView.indexPathForItem(at: visiblePoint) {
            currentIndex = visibleIndexPath.row
        }
    }
}
