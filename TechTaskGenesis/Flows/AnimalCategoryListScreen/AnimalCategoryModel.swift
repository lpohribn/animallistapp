//
//  AnimalCategoryModel.swift
//  TechTaskGenesis
//
//  Created by Liudmila on 10.02.2022.
//

import Foundation

struct Animal: Codable {
    
    var statusType: StatusType?
    let title: String
    let description: String
    let image: String
    let order: Int
    let status: String
    let content: [Content]?

    enum CodingKeys: String, CodingKey {
        case title
        case description
        case image
        case order
        case status
        case content
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decode(String.self, forKey: .title)
        description = try values.decode(String.self, forKey: .description)
        image = try values.decode(String.self, forKey: .image)
        order = try values.decode(Int.self, forKey: .order)
        status = try values.decode(String.self, forKey: .status)
        statusType = StatusType(rawValue: status) ?? .free
        content = try values.decodeIfPresent([Content].self, forKey: .content)
    }
    
}

enum StatusType: String {
    case paid
    case free
}

struct Content: Codable {
    let fact: String
    let image: String
}

