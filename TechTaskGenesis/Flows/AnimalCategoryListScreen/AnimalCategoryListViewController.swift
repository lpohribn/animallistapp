//
//  AnimalCategoryListViewController.swift
//  TechTaskGenesis
//
//  Created by Liudmila on 10.02.2022.
//

import UIKit

final class AnimalCategoryListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private var viewModel: AnimalCategoryViewModel = AnimalCategoryViewModel(networkService: NetworkService())
    
//    private var viewModel: AnimalCategoryViewModel?
    
    enum Status: String {
        case free
        case paid
        case unavailable
        case error
    }
    
    func setViewModel(viewModel: AnimalCategoryViewModel) {
        self.viewModel = viewModel
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
            self.activityIndicator.startAnimating()
        self.viewModel.getAnimalData(with: "https://drive.google.com/uc?export=download&id=12L7OflAsIxPOF47ssRdKyjXoWbUrq4V5")
        registerCells()
    }
    
    private func showAlert(text: String, status: Status, indexPath: IndexPath? = nil) {
        let alert = UIAlertController(title: text, message: "", preferredStyle: UIAlertController.Style.alert)
        switch status {
        case .paid:
            alert.addAction(UIAlertAction(title: "Show Ad", style: .cancel, handler: { (action: UIAlertAction!) in
                self.activityIndicator.startAnimating()
                self.view.isUserInteractionEnabled = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.activityIndicator.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                    if let indexPath = indexPath {
                        self.goToFacts(indexPath: indexPath)
                    }
                }
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        case .unavailable, .error:
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        default:
            return
        }
        self.present(alert, animated: true, completion: nil)
        
    }
    
    private func setupUI() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        viewModel.delegate = self
    }
    
    private func goToFacts(indexPath: IndexPath) {
        let factViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "factsViewControllerIdentifier", creator: { coder in
            return FactsViewController(coder: coder, viewModel: FactsViewModel(array: self.viewModel.getAnimalFacts(indexPath), networkService: NetworkService()))
        }) as! FactsViewController
        self.navigationController?.pushViewController(factViewController, animated: true)
    }
}

extension AnimalCategoryListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func registerCells() {
        tableView.register(UINib(nibName: "AnimalTableViewCell", bundle: nil), forCellReuseIdentifier: "animalTableViewCellIdentifier")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "animalTableViewCellIdentifier", for: indexPath) as? AnimalTableViewCell else { return UITableViewCell() }
        cell.setAnimalData(animal: viewModel.itemAtIndexPath(indexPath))
        viewModel.getImageData(with: viewModel.itemAtIndexPath(indexPath).image ?? "") { imgData, error in
                if let imgData = imgData {
                    DispatchQueue.main.async {
                        cell.animalUImageView.image = UIImage(data: imgData)
                        self.activityIndicator.stopAnimating()
                    }
                }
            }
            return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let _ = viewModel.itemAtIndexPath(indexPath).content else {
            showAlert(text: "Coming soon", status: .unavailable)
            return
        }
        switch viewModel.itemAtIndexPath(indexPath).statusType {
        case .free:
            goToFacts(indexPath: indexPath)
        case .paid:
            showAlert(text: "Watch Ad to continue", status: .paid, indexPath: indexPath)
        default:
            return
        }
    }
}

extension AnimalCategoryListViewController: AnimalListViewModelDelegate {
    
    func updateData() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.hidesWhenStopped = true
            self.tableView.reloadData()
        }
    }
    
    func showError(_ error: Error) {
        showAlert(text: "Error", status: .error)
    }
    
}
