//
//  AnimalCategoryViewModel.swift
//  TechTaskGenesis
//
//  Created by Liudmila on 10.02.2022.
//

import Foundation

protocol AnimalListViewModelDelegate: AnyObject {
    func updateData()
    func showError(_ error: Error)
}

final class AnimalCategoryViewModel {
    
    weak var delegate: AnimalListViewModelDelegate?
    private let networkService: NetworkService?
    private var animals: [Animal] = []
    
    init(networkService: NetworkService) {
        self.networkService = networkService
    }
    
    func getAnimalData(with url: String) {
        
        DispatchQueue.global(qos: .userInitiated).async {
            self.networkService?.getAnimalList(urlString: url) { [weak self] animals, error in
                if let animals = animals {
                    self?.animals = animals
                    self?.delegate?.updateData()
                } else if let error = error {
                    self?.delegate?.showError(error)
                }
            }
        }
    }
    
    func sortByOrder() -> [Animal] {
        return animals.sorted {$0.order < $1.order}
    }
    
    func getImageData(with url: String, completion: @escaping (Data?, Error?) -> Void) {
        networkService?.getImage(urlString: url) { imageData, error in
            if let data = imageData {
                completion(data, nil)
            } else {
                completion(nil, error)
            }
        }
    }
    
    func numberOfRows() -> Int {
        return animals.count
    }
    
    func itemAtIndexPath(_ indexPath: IndexPath) -> Animal {
        return animals[indexPath.row]
    }
    
    func getAnimalFacts(_ indexPath: IndexPath) -> [Content]? {
        return animals[indexPath.row].content
    }
    
}
