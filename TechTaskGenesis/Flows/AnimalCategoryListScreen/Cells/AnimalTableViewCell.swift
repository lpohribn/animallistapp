//
//  AnimalTableViewCell.swift
//  TechTaskGenesis
//
//  Created by Liudmila on 10.02.2022.
//

import UIKit

final class AnimalTableViewCell: UITableViewCell {
    
    @IBOutlet weak var animalUImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var premiumLabel: UILabel!
    @IBOutlet weak var comingSoonImage: UIImageView!
    @IBOutlet weak var mainView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    private func setupUI() {
        titleLabel.numberOfLines = 0
        subtitleLabel.numberOfLines = 0
        backgroundColor = .clear
        mainView.layer.cornerRadius = 10
        selectionStyle = .none
    }
    
    func setAnimalData(animal: Animal?) {
        titleLabel.text = animal?.title
        subtitleLabel.text = animal?.description
        if animal?.statusType  == .paid {
            premiumLabel.text = "Premium"
        }
        if animal?.content == nil {
            mainView.backgroundColor = .white
            mainView.alpha = 0.5
            comingSoonImage.image = UIImage(named: "comingSoon")
        }
    }
}
