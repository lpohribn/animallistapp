//
//  NetworkService.swift
//  TechTaskGenesis
//
//  Created by Liudmila on 10.02.2022.
//

import Foundation

class NetworkService {
    
    func getAnimalList(urlString: String, completion: @escaping ([Animal]?, Error?) -> Void) {
        guard let url = URL(string: urlString) else { return }
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: url) { data, _, error in
            if let data = data {
                if let arrayOfAnimal = self.parseJSON(data: data) {
                    completion(arrayOfAnimal, nil)
                }
            }
            if let error = error {
                completion(nil, error)
            }
        }
        task.resume()
    }
    
    func getImage(urlString: String, completion: @escaping (Data?, Error?) -> Void) {
        guard let url = URL(string: urlString) else { return }
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: url) { data, _, error in
            if let data = data {
                completion(data, nil)
            }
            if let error = error {
                completion(nil, error)
            }
        }
        task.resume()
    }
    
    private func parseJSON(data: Data) -> [Animal]? {
        let decoder = JSONDecoder()
        do {
            let arrayData = try decoder.decode([Animal].self, from: data)
            return arrayData
            
        } catch let DecodingError.dataCorrupted(context) {
            print(context)
            return []
        } catch let DecodingError.keyNotFound(key, context) {
            print("Key '\(key)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
            return []
        } catch let DecodingError.valueNotFound(value, context) {
            print("Value '\(value)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
            return []
        } catch let DecodingError.typeMismatch(type, context)  {
            print("Type '\(type)' mismatch:", context.debugDescription)
            print("codingPath:", context.codingPath)
            return []
        } catch {
            print("error: ", error)
            return []
        }
    }
}
